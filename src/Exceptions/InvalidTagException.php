<?php

namespace DraperStudio\Taggable\Exceptions;

/**
 * Class InvalidTagException.
 */
class InvalidTagException extends \InvalidArgumentException
{
    //
}
